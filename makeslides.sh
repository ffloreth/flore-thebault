#!/usr/bin/env bash
set -ex
for file in $(find modules/slides/attachments/ -maxdepth 2 -name '*.adoc')
do
  npx asciidoctor-revealjs --attribute revealjsdir=https://cdn.jsdelivr.net/npm/reveal.js@5.1.0 --require asciidoctor-kroki --attribute kroki-fetch-diagram=true $file
done
